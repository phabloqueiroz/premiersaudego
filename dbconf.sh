sudo apt-get install mysql-server
# root pwd 'premier'
mysql_secure_installation
mysql> CREATE USER 'webapp'@'localhost' IDENTIFIED BY 'premier';
mysql> GRANT ALL PRIVILEGES ON * . * TO 'webapp'@'localhost' ;
mysql> flush privileges;

#forcar DELETE CASCADE na tabela preco
mysql>
alter table `preco`
drop FOREIGN KEY `preco_ibfk_1` ;
alter table `preco`
add constraint FOREIGN KEY (`tabela_precos_id`) REFERENCES `tabela_precos` (`id`) ON DELETE CASCADE;