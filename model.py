# coding: utf-8
import enum

from passlib.hash import pbkdf2_sha256 as sha256

from project import db

db.metadata.clear()


class PessoaFisica(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(250), nullable=False)
    cpf = db.Column(db.String(11), nullable=False)
    data_nascimento = db.Column(db.Date)
    email = db.Column(db.String(250))
    telefones = db.relationship('Telefone', back_populates='pessoa_fisica',
                                cascade='save-update, merge, delete, delete-orphan')

    __mapper_args__ = {
        'polymorphic_identity': 'pessoa_fisica',
    }

    def __init__(self, nome, cpf, data_nascimento=None, email=None, telefones=[]):
        self.nome = nome
        self.cpf = cpf
        self.data_nascimento = data_nascimento
        self.email = email
        self.telefones = telefones


class Telefone(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    numero = db.Column(db.String(11), nullable=False)
    pessoa_fisica_id = db.Column(db.Integer, db.ForeignKey('pessoa_fisica.id'), nullable=False)
    pessoa_fisica = db.relationship('PessoaFisica', back_populates='telefones')

    def __init__(self, numero, pessoa_fisica=None):
        self.numero = numero
        self.pessoa_fisica = pessoa_fisica


# Usuarios/Perfis de acesso
class PerfilEnum(enum.IntEnum):
    ADMINISTRADOR = 3
    GESTOR = 2
    OPERACIONAL = 1


class Usuario(db.Model):
    login = db.Column(db.String(120), nullable=False, primary_key=True)
    ativo = db.Column(db.Boolean, nullable=False)
    perfil = db.Column(db.Enum(PerfilEnum), nullable=False)
    senha = db.Column(db.String(120), nullable=False)
    pessoa_fisica = db.relationship('PessoaFisica')
    pessoa_fisica_id = db.Column(db.Integer, db.ForeignKey('pessoa_fisica.id'), nullable=False)

    def __init__(self, perfil, login, senha, pessoa_fisica, ativo=True):
        self.ativo = ativo
        self.login = login
        self.senha = senha
        self.perfil = perfil
        self.pessoa_fisica = pessoa_fisica

    @staticmethod
    def gera_hash(senha):
        return sha256.hash(senha)

    @staticmethod
    def verifica_hash(senha, hash):
        return sha256.verify(senha, hash)


class TokenRevogado(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    @classmethod
    def foi_revogado(cls, jti):
        return bool(cls.query.filter_by(jti=jti).first())


# Produtos
class Operadora(db.Model):
    __tablename__ = 'operadora'

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(150), nullable=False)
    ativo = db.Column(db.Boolean, nullable=False)
    produtos = db.relationship('Produto', back_populates='operadora')

    def __init__(self, nome, ativo=True):
        self.nome = nome
        self.ativo = ativo


class FaixaEtaria(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    idade_minima = db.Column(db.Integer, nullable=False)
    idade_maxima = db.Column(db.Integer)

    def __init__(self, idade_minima, idade_maxima=None):
        self.idade_minima = idade_minima
        self.idade_maxima = idade_maxima

    @property
    def descricao(self):
        if self.idade_maxima:
            return str(self.idade_minima) + ' a ' + str(self.idade_maxima)

        return str(self.idade_minima) + ' acima'


class AcomodacaoEnum(enum.IntEnum):
    APARTAMENTO = 1
    ENFERMARIA = 2


class Produto(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(150), nullable=False)
    ativo = db.Column(db.Boolean, nullable=False)
    operadora_id = db.Column(db.Integer, db.ForeignKey('operadora.id'), nullable=False)
    operadora = db.relationship('Operadora', back_populates='produtos')
    vidas = db.Column(db.Integer, nullable=False)
    acomodacao = db.Column(db.Enum(AcomodacaoEnum), nullable=False)
    obstetricia_inclusa = db.Column(db.Boolean, nullable=False)
    tabelas_precos = db.relationship('TabelaPrecos', back_populates='produto', foreign_keys='[TabelaPrecos.produto_id]')

    def __init__(self, nome, operadora, acomodacao, obstetricia_inclusa, vidas=1, ativo=True, tabelas_precos=[]):
        self.nome = nome
        self.ativo = ativo
        self.operadora = operadora
        self.vidas = vidas
        self.acomodacao = acomodacao
        self.obstetricia_inclusa = obstetricia_inclusa
        self.tabelas_precos = tabelas_precos

    @property
    def descricao(self):
        return self.nome + ' ({0})'.format(', '.join((
            'Individual' if self.vidas == 1 else 'Familiar {0} vidas'.format(str(self.vidas)),
            self.acomodacao.name.capitalize(),
            'com Obstetrícia' if self.obstetricia_inclusa else 'sem Obstetrícia')))


class TabelaPrecos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    descricao = db.Column(db.String(50))
    produto_id = db.Column(db.Integer, db.ForeignKey('produto.id'), nullable=False)
    produto = db.relationship('Produto', back_populates='tabelas_precos', foreign_keys='[TabelaPrecos.produto_id]')
    precos = db.relationship('Preco', back_populates='tabela_precos', foreign_keys='Preco.tabela_precos_id',
                             cascade='save-update, merge, delete, delete-orphan')
    data_criacao = db.Column(db.Date)
    ativa = db.Column(db.Boolean, nullable=False)

    def __init__(self, produto, precos, data_criacao=None, descricao=None, ativa=True):
        self.descricao = descricao
        self.produto = produto
        self.precos = precos
        self.data_criacao = data_criacao
        self.ativa = ativa


class Preco(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    preco = db.Column(db.Float, nullable=False)
    tabela_precos_id = db.Column(db.Integer, db.ForeignKey('tabela_precos.id'), nullable=False)
    tabela_precos = db.relationship('TabelaPrecos')
    faixa_etaria_id = db.Column(db.Integer, db.ForeignKey('faixa_etaria.id'), nullable=False)
    faixa_etaria = db.relationship('FaixaEtaria')

    def __init__(self, preco, faixa_etaria, produto=None):
        self.preco = preco
        self.produto = produto
        self.faixa_etaria = faixa_etaria


# Vendas
class Vendedor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ativo = db.Column(db.Boolean, nullable=False)
    vendas = db.relationship('Venda', back_populates='vendedor')
    pessoa_fisica_id = db.Column(db.Integer, db.ForeignKey('pessoa_fisica.id'), nullable=False)
    pessoa_fisica = db.relationship('PessoaFisica')

    def __init__(self, pessoa_fisica=None, ativo=True):
        self.pessoa_fisica = pessoa_fisica
        self.ativo = ativo


class StatusVendaEnum(enum.IntEnum):
    CONFIRMADA = 1
    CANCELADA = 2


class Venda(db.Model):
    adesao = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.Date, nullable=False)
    taxa_adesao = db.Column(db.Float, nullable=False)
    produto_id = db.Column(db.Integer, db.ForeignKey('produto.id'), nullable=False)
    produto = db.relationship('Produto', foreign_keys=[produto_id])
    vendedor_id = db.Column(db.Integer, db.ForeignKey('vendedor.id'), nullable=False)
    vendedor = db.relationship('Vendedor', back_populates='vendas', foreign_keys=[vendedor_id])
    responsavel_id = db.Column(db.Integer, db.ForeignKey('pessoa_fisica.id'), nullable=False)
    responsavel = db.relationship('PessoaFisica', foreign_keys=[responsavel_id])
    observacao = db.Column(db.String(500))
    beneficiarios = db.relationship('Beneficiario', foreign_keys='Beneficiario.venda_adesao', back_populates='venda',
                                    cascade="all, delete-orphan")
    status = db.Column(db.Enum(StatusVendaEnum), nullable=False)
    comissionada = db.Column(db.Boolean, nullable=False)

    def __init__(self, adesao, data, taxa_adesao, produto, vendedor, responsavel=None, beneficiarios=[],
                 observacao=None, status=StatusVendaEnum.CONFIRMADA, comissionada=True):
        self.adesao = adesao
        self.data = data
        self.taxa_adesao = taxa_adesao
        self.produto = produto
        self.vendedor = vendedor
        self.responsavel = responsavel
        self.beneficiarios = beneficiarios
        self.observacao = observacao
        self.status = status
        self.comissionada = comissionada

    @property
    def titular(self):
        return [beneficiario for beneficiario in self.beneficiarios if beneficiario.e_titular].pop()

    @property
    def dependentes(self):
        return [beneficiario for beneficiario in self.beneficiarios if not beneficiario.e_titular]


class Beneficiario(PessoaFisica):
    id = db.Column(db.Integer, db.ForeignKey('pessoa_fisica.id'), primary_key=True)
    preco = db.Column(db.Float, nullable=False)
    e_titular = db.Column(db.Boolean, nullable=False)
    venda_adesao = db.Column(db.Integer, db.ForeignKey('venda.adesao'), nullable=False)
    venda = db.relationship('Venda', back_populates='beneficiarios', cascade="all, delete")

    __mapper_args__ = {
        'polymorphic_identity': 'beneficiario',
    }

    def __init__(self, nome, cpf, data_nascimento, preco, e_titular, venda=None, email=None, telefones=[]):
        super(Beneficiario, self).__init__(nome=nome, cpf=cpf, data_nascimento=data_nascimento, email=email,
                                           telefones=telefones)
        self.preco = preco
        self.e_titular = e_titular
        self.venda = venda


if __name__ == '__main__':
    db.session.execute("DROP SCHEMA premiersaudego;")
    db.session.execute("CREATE DATABASE premiersaudego;")
    db.session.execute("USE premiersaudego;")
    db.session.commit()

    db.create_all()
