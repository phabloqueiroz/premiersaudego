#!-*- conding: utf8 -*-
from project import db
from model import *


#print(Usuario.query.filter(Usuario.id == int(1)).first().nome)



#promed = Operadora(nome='Promed')
#saudegoiania = Operadora(nome='Saúde Goiânia')
#odonto = Operadora(nome='Odonto')

#db.session.add_all([america, promed, saudegoiania, odonto])
#db.session.commit()

_00_18 = FaixaEtaria(idade_minima=0, idade_maxima=18)
_19_23 = FaixaEtaria(idade_minima=19, idade_maxima=23)
_24_28 = FaixaEtaria(idade_minima=24, idade_maxima=28)
_29_33 = FaixaEtaria(idade_minima=29, idade_maxima=33)
_34_38 = FaixaEtaria(idade_minima=34, idade_maxima=38)
_39_43 = FaixaEtaria(idade_minima=39, idade_maxima=43)
_44_48 = FaixaEtaria(idade_minima=44, idade_maxima=48)
_49_53 = FaixaEtaria(idade_minima=49, idade_maxima=53)
_54_58 = FaixaEtaria(idade_minima=54, idade_maxima=58)
_59 = FaixaEtaria(idade_minima=59)

db.session.add_all([_00_18, _19_23, _24_28, _29_33, _34_38,
                    _39_43, _44_48, _49_53, _54_58, _59])

america = Operadora(nome='América')
db.session.add(america)

db.session.add(Produto(nome='Esmeralda Copart', operadora=america, acomodacao=AcomodacaoEnum.APARTAMENTO,
                       obstetricia_inclusa=False, vidas=1))

db.session.commit()

#produto_esmeralda_copart = Produto()
#db.session.add(produto_esmeralda_copart)

#
#produto_esmeralda_copart_precos = []
#produto_esmeralda_copart_precos.append(Preco(preco=174.00, faixa_etaria=_00_18))
#produto_esmeralda_copart_precos.append(Preco(preco=205.33, faixa_etaria=_19_23))
#produto_esmeralda_copart_precos.append(Preco(preco=236.12, faixa_etaria=_24_28))
#produto_esmeralda_copart_precos.append(Preco(preco=278.63, faixa_etaria=_29_33))
#produto_esmeralda_copart_precos.append(Preco(preco=320.42, faixa_etaria=_34_38))
#produto_esmeralda_copart_precos.append(Preco(preco=368.48, faixa_etaria=_39_43))
#produto_esmeralda_copart_precos.append(Preco(preco=423.76, faixa_etaria=_44_48))
#produto_esmeralda_copart_precos.append(Preco(preco=529.69, faixa_etaria=_49_53))
#produto_esmeralda_copart_precos.append(Preco(preco=688.60, faixa_etaria=_54_58))
#produto_esmeralda_copart_precos.append(Preco(preco=964.04, faixa_etaria=_59))
#
#tabpreco_esmeralda_copart = TabelaPreco(produto=produto_esmeralda_copart, precos=produto_esmeralda_copart_precos, vigencia_inicio='2018-07-16')
#db.session.add(tabpreco_esmeralda_copart)
#db.session.commit()