from flask import jsonify, request
from flask_restful import Resource
from marshmallow import ValidationError

from project import Operadora, db, Produto, TabelaPrecos
from ..schema import OperadoraSchema, ProdutoSchema


class OperadoraResource(Resource):

    def get(self, id=None):
        if not id:
            somente_ativas = request.args.get('somenteAtivas')
            if somente_ativas == 'true':
                operadoras = Operadora.query.filter(Operadora.ativo == True).order_by(
                    Operadora.nome.asc())
            else:
                operadoras = Operadora.query.order_by(Operadora.nome.asc()).all()

            return jsonify(OperadoraSchema(many=True, exclude=('produtos',)).dump(operadoras))

        return jsonify(OperadoraSchema(exclude=('produtos',)).dump(Operadora.query.get(id)))

    def put(self):
        operadora_schema = OperadoraSchema(partial=('id',), exclude=('produtos',))
        try:
            operadora = operadora_schema.load(request.json)

        except ValidationError as err:
            return {'message': err.messages}, 401

        db.session.commit()

    def post(self):
        operadora_schema = OperadoraSchema(partial=('id',), exclude=('produtos',))
        try:
            nova_operadora = operadora_schema.load(request.json)

        except ValidationError as err:
            return {'message': err.messages}, 400

        db.session.add(nova_operadora)
        db.session.commit()


class OperadoraProdutoResource(Resource):

    def get(self, operadora_id):
        somente_com_tabela_preco_ativa = request.args.get('somenteComTabelaPrecoAtiva')
        if somente_com_tabela_preco_ativa and somente_com_tabela_preco_ativa == 'true':
            produtos = Produto.query \
                .join(Operadora, Operadora.id == Produto.operadora_id) \
                .join(TabelaPrecos, TabelaPrecos.produto_id == Produto.id) \
                .filter(Operadora.id == operadora_id,
                        Operadora.ativo == True,
                        Produto.ativo == True,
                        TabelaPrecos.ativa == True)

        else:
            produtos = Produto.query \
                .join(Operadora, Operadora.id == Produto.operadora_id) \
                .filter(Operadora.id == operadora_id,
                        Operadora.ativo == True,
                        Produto.ativo == True)

        return ProdutoSchema(only=('id', 'descricao', 'vidas',), many=True).dump(produtos)
