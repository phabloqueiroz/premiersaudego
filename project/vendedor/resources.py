from flask import request
from flask_restful import Resource
from marshmallow import ValidationError

from project import Vendedor, PessoaFisica, db
from ..schema import VendedorSchema


class VendedorResource(Resource):

    def get(self, id=None):
        if not id:
            somente_ativos = request.args.get('somenteAtivos')
            if somente_ativos == 'true':
                vendedores = Vendedor.query \
                    .join(PessoaFisica, PessoaFisica.id == Vendedor.pessoa_fisica_id) \
                    .filter(Vendedor.ativo == True) \
                    .order_by(PessoaFisica.nome.asc()).all()
            else:
                vendedores = Vendedor.query \
                    .join(PessoaFisica, PessoaFisica.id == Vendedor.pessoa_fisica_id) \
                    .order_by(Vendedor.ativo.desc(), PessoaFisica.nome.asc()).all()

            return VendedorSchema(many=True, exclude=('vendas',)).dump(vendedores)

        return VendedorSchema(exclude=('vendas',)).dump(Vendedor.query.get(id))

    def put(self):
        vendedor_schema = VendedorSchema()
        try:
            vendedor = vendedor_schema.load(request.json)

            if not vendedor.pessoa_fisica.id:
                raise ValidationError('Missing pessoa_fisica.id field')

        except ValidationError as err:
            return {'message': err.messages}, 401

        db.session.commit()

    def post(self):
        vendedor_schema = VendedorSchema(exclude=('vendas','id', 'pessoa_fisica.id',))
        try:
            novo_vendedor = vendedor_schema.load(request.json)

        except ValidationError as err:
            return {'message': err.messages}, 401

        if Vendedor.query.join(PessoaFisica, Vendedor.pessoa_fisica_id == PessoaFisica.id) \
                .filter(PessoaFisica.cpf == novo_vendedor.pessoa_fisica.cpf).first():
            return {'message': 'Já existe um usuário cadastrado para o CPF informado'}, 401

        pessoa_fisica_ja_existente = PessoaFisica.query.filter(
            PessoaFisica.cpf == novo_vendedor.pessoa_fisica.cpf).first()

        if pessoa_fisica_ja_existente:
            pessoa_fisica_ja_existente.email = novo_vendedor.pessoa_fisica.email
            pessoa_fisica_ja_existente.nome = novo_vendedor.pessoa_fisica.nome
            pessoa_fisica_ja_existente.data_nascimento = novo_vendedor.pessoa_fisica.data_nascimento
            db.session.commit()
            novo_vendedor.pessoa_fisica = pessoa_fisica_ja_existente

        db.session.add(novo_vendedor)
        db.session.commit()
