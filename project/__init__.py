from functools import wraps

import flask_excel as excel
from flask import Flask, Blueprint
from flask_cors import CORS
from flask_jwt_extended import JWTManager, verify_jwt_in_request, get_jwt_claims
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

# #### config ####
app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('flask.cfg')

CORS(app)
excel.init_excel(app)

db = SQLAlchemy(app)
from model import *

# ### JWT config ###
jwt = JWTManager(app)


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return TokenRevogado.foi_revogado(jti)


def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if PerfilEnum.ADMINISTRADOR.name not in claims['perfis']:
            return {'message': 'Acesso negado'}, 403
        else:
            return fn(*args, **kwargs)

    return wrapper


def gestor_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if PerfilEnum.GESTOR.name not in claims['perfis']:
            return {'message': 'Acesso negado'}, 403
        else:
            return fn(*args, **kwargs)

    return wrapper


@jwt.user_claims_loader
def add_claims_to_access_token(identity):
    return {'perfis': [perfilEnum.name for perfilEnum in list(PerfilEnum) if
                       perfilEnum.value <= Usuario.query.get(identity).perfil.value]}


# ### vars ###

HEADER = {'Content-Type': 'application/json'}
RESPONSE_404 = ({'message': 'Elemento nao encontrado'}, 404, HEADER)

# ### resources ###
# Usuarios
from project.usuario.resources import UsuarioResource, LoginResource, TokenRefreshResource, LogoutAccessResource, \
    LogoutRefreshResource

usuario_blueprint = Blueprint('usuario', __name__)
usuario_api = Api(usuario_blueprint)
usuario_api.add_resource(UsuarioResource, '/usuario',
                         '/usuario/<int:id>')
usuario_api.add_resource(LoginResource, '/login')
usuario_api.add_resource(TokenRefreshResource, '/login/refresh')
usuario_api.add_resource(LogoutAccessResource, '/logout/access')
usuario_api.add_resource(LogoutRefreshResource, '/logout/refresh')
app.register_blueprint(usuario_blueprint)

# Operadoras
from project.operadora.resources import OperadoraResource, OperadoraProdutoResource

operadora_blueprint = Blueprint('operadora', __name__)
operadora_api = Api(operadora_blueprint)
operadora_api.add_resource(OperadoraResource, '/operadoras',
                           '/operadoras/<int:id>')
operadora_api.add_resource(OperadoraProdutoResource, '/operadoras/<int:operadora_id>/produtos')
app.register_blueprint(operadora_blueprint)

# Produtos
from project.produto.resources import ProdutoResource, ProdutoTabelaPrecosResource
from project.preco.resources import PrecoResource

produto_blueprint = Blueprint('produto', __name__)
produto_api = Api(produto_blueprint)
produto_api.add_resource(ProdutoResource, '/produtos',
                         '/produtos/<int:id>')
produto_api.add_resource(ProdutoTabelaPrecosResource, '/produtos/<int:produto_id>/tabelas-precos')
produto_api.add_resource(PrecoResource, '/produtos/<int:produto_id>/idade/<int:idade>')
app.register_blueprint(produto_blueprint)

from project.tabela_precos.resources import TabelaPrecosResource

tabelaPrecos_blueprint = Blueprint('tabela_preco', __name__)
tabelaPrecos_api = Api(tabelaPrecos_blueprint)
tabelaPrecos_api.add_resource(TabelaPrecosResource, '/tabelas-precos',
                              '/tabelas-precos/<int:id>')
app.register_blueprint(tabelaPrecos_blueprint)

# Vendedores
from project.vendedor.resources import VendedorResource

vendedor_blueprint = Blueprint('vendedor', __name__)
vendedor_api = Api(vendedor_blueprint)
vendedor_api.add_resource(VendedorResource, '/vendedores',
                          '/vendedores/<int:id>')
app.register_blueprint(vendedor_blueprint)

# Vendas
from project.venda.resources import VendaResource, VendaStatusResource, Venda2Resource

venda_blueprint = Blueprint('venda', __name__)
venda_api = Api(venda_blueprint)
venda_api.add_resource(VendaResource, '/vendas',
                       '/vendas/<int:adesao>')
venda_api.add_resource(VendaStatusResource, '/vendas/<int:adesao>/status')
venda_api.add_resource(Venda2Resource, '/vendas/adesao/<int:adesao>')
app.register_blueprint(venda_blueprint)

# Faixas Etárias
from project.faixa_etaria.resources import FaixaEtariaResource

faixa_etaria_blueprint = Blueprint('faixa_etaria', __name__)
faixa_etaria_api = Api(faixa_etaria_blueprint)
faixa_etaria_api.add_resource(FaixaEtariaResource, '/faixas-etarias')
app.register_blueprint(faixa_etaria_blueprint)

# Relatorios
from project.relatorio.resources import RelatorioVendasResource, RelatorioVendasPorOperadoraResource, \
    RelatorioVendasPorVendedorResource

relatorio_blueprint = Blueprint('relatorio', __name__)
relatorio_api = Api(relatorio_blueprint)
venda_api.add_resource(RelatorioVendasResource, '/relatorios/vendas-totais')
venda_api.add_resource(RelatorioVendasPorOperadoraResource, '/relatorios/vendas-por-operadora')
venda_api.add_resource(RelatorioVendasPorVendedorResource, '/relatorios/vendas/vendedor/<int:vendedor_id>')
app.register_blueprint(relatorio_blueprint)
