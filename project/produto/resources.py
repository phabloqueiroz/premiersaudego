import flask
from flask import request
from flask_restful import Resource
from marshmallow import ValidationError

from project import Produto, db, TabelaPrecos, Operadora
from ..schema import ProdutoSchema, TabelaPrecosSchema


class ProdutoResource(Resource):

    def get(self, id=None):
        if not id:
            somente_ativos = request.args.get('somenteAtivos')
            if somente_ativos == 'true':
                produtos = Produto.query.join(Operadora, Produto.operadora_id == Operadora.id, Operadora.ativo == True) \
                    .filter(Produto.ativo == True) \
                    .order_by(Operadora.nome.asc(), Produto.nome.asc(), Produto.vidas.asc())
            else:
                produtos = Produto.query.join(Operadora, Produto.operadora_id == Operadora.id) \
                    .order_by(Operadora.nome.asc(), Produto.nome.asc(), Produto.vidas.asc()).all();

            return ProdutoSchema(many=True, only=('id', 'descricao', 'operadora.nome', 'ativo',)).dump(produtos)

        return ProdutoSchema(exclude=('tabelas_precos',)).dump(Produto.query.get(id))

    def put(self):
        produto_schema = ProdutoSchema(exclude=('tabelas_precos', 'operadora.nome',))
        try:
            produto = produto_schema.load(flask.request.json)

        except ValidationError as err:
            return {'message': err.messages}, 400

        db.session.commit()

    def post(self):
        produto_schema = ProdutoSchema(partial=('id',), exclude=('tabelas_precos', 'operadora.nome',))
        try:
            novo_produto = produto_schema.load(flask.request.json)

        except ValidationError as err:
            return {'message': err.messages}, 400

        db.session.add(novo_produto)
        db.session.commit()


class ProdutoTabelaPrecosResource(Resource):

    def get(self, produto_id):
        if not request.args.get('somente_ativas'):
            return TabelaPrecosSchema(many=True).dump(
                TabelaPrecos.query.filter(TabelaPrecos.produto_id == produto_id).order_by(TabelaPrecos.ativa.desc(),
                                                                                          TabelaPrecos.data_criacao.desc()).all()
            )

        return TabelaPrecosSchema().dump(
            TabelaPrecos.query.filter(
                TabelaPrecos.produto_id == produto_id,
                TabelaPrecos.ativa == True).first())
