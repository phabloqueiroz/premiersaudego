from flask import jsonify, request
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, \
    get_jwt_identity, get_raw_jwt
from flask_restful import Resource
from marshmallow import ValidationError
from sqlalchemy import or_, and_

from project import Usuario, PessoaFisica, TokenRevogado, db, gestor_required, PerfilEnum
from ..schema import UsuarioSchema


class UsuarioResource(Resource):

    def get(self, id=None):
        if not id:
            return jsonify(usuarios=UsuarioSchema(many=True).dumps(Usuario.query.all()).data)

        return UsuarioSchema().dumps(Usuario.query.get(id)).data

    def put(self):
        usuario_schema = UsuarioSchema()
        try:
            usuario = usuario_schema.load(request.json)

            if not usuario.pessoa_fisica.id:
                raise ValidationError('Pessoa física não informada.')

            if usuario.perfil == PerfilEnum.ADMINISTRADOR and not usuario.ativo:
                raise ValidationError('Não é possível desativar usuário ADMINISTRADOR.')

        except ValidationError as err:
            return {'message': err.messages}, 401

        db.session.commit()

    def post(self):
        usuario_schema = UsuarioSchema(partial=('id', 'ativo',))
        try:
            novo_usuario = usuario_schema.load(request.json)
            if novo_usuario.perfil == PerfilEnum.ADMINISTRADOR:
                novo_usuario.ativo = True

        except ValidationError as err:
            return {'message': err.messages}, 401

        if Usuario.query.get(novo_usuario.login):
            return {'message': 'Login {} já cadatrado'.format(novo_usuario.login)}, 409

        if Usuario.query.join(PessoaFisica, Usuario.pessoa_fisica_id == PessoaFisica.id).filter(
                or_(PessoaFisica.email == novo_usuario.pessoa_fisica.email,
                    PessoaFisica.cpf == novo_usuario.pessoa_fisica.cpf)).first():
            return {'message': 'Já existe um usuário cadastrado para o CPF e/ou email informados'}, 409

        pessoa_fisica_ja_existente = PessoaFisica.query.filter(
            PessoaFisica.cpf == novo_usuario.pessoa_fisica.cpf).first()

        if pessoa_fisica_ja_existente:
            pessoa_fisica_ja_existente.email = novo_usuario.pessoa_fisica.email
            pessoa_fisica_ja_existente.nome = novo_usuario.pessoa_fisica.nome
            if novo_usuario.pessoa_fisica.data_nascimento:
                pessoa_fisica_ja_existente.data_nascimento = novo_usuario.pessoa_fisica.data_nascimento
            db.session.commit()
            novo_usuario.pessoa_fisica = pessoa_fisica_ja_existente

        db.session.add(novo_usuario)
        db.session.commit()


class LoginResource(Resource):

    def post(self):
        usuario_login = UsuarioSchema(
            only=('login', 'senha',)).load(request.json)

        usuario_banco = Usuario.query.filter(
            and_(
                Usuario.login == usuario_login.login,
                Usuario.senha == usuario_login.senha)).first()

        if not usuario_banco:
            return {'message': 'Usuário e/ou senha inválido(s)'}, 403

        if not usuario_banco.ativo:
            return {'message': 'Usuário desativado'}, 403

        return {'access_token': create_access_token(identity=usuario_banco.login),
                'refresh_token': create_refresh_token(identity=usuario_banco.login)}

    @gestor_required
    def get(self):
        return {'message': 'OK'}


class LogoutAccessResource(Resource):

    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']

        db.session.add(TokenRevogado(jti=jti))
        db.session.commit()


class LogoutRefreshResource(Resource):

    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()['jti']

        db.session.add(TokenRevogado(jti=jti))
        db.session.commit()


class TokenRefreshResource(Resource):

    @jwt_refresh_token_required
    def post(self):
        return {'access_token': create_access_token(identity=get_jwt_identity())}
