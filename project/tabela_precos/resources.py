from datetime import datetime

from flask import request
from flask_restful import Resource
from marshmallow import ValidationError
from sqlalchemy import exc

from project import TabelaPrecos, db
from ..schema import TabelaPrecosSchema


class TabelaPrecosResource(Resource):

    def get(self, id):
        return TabelaPrecosSchema().dump(TabelaPrecos.query.get(id))

    def put(self):
        tabela_precos_schema = TabelaPrecosSchema()
        try:
            tabela_precos = tabela_precos_schema.load(request.json)

            if tabela_precos.ativa:
                TabelaPrecos.query.filter(
                    TabelaPrecos.produto_id == tabela_precos.produto.id,
                    TabelaPrecos.ativa == True,
                    TabelaPrecos.id != tabela_precos.id).update({'ativa': False})
                tabela_precos.ativa = True

        except ValidationError as err:
            return {'message': err.messages}, 401

        except exc.IntegrityError as err:
            return {'message': str(err)}, 401

        db.session.commit()

    def post(self):
        tabela_precos_schema = TabelaPrecosSchema(partial=('id','data_criacao',))
        try:
            nova_tabela_precos = tabela_precos_schema.load(request.json)

        except ValidationError as err:
            return {'message': err.messages}, 401

        nova_tabela_precos.data_criacao = datetime.now()
        db.session.add(nova_tabela_precos)
        db.session.commit()

        if nova_tabela_precos.ativa:
            TabelaPrecos.query.filter(
                TabelaPrecos.produto_id == nova_tabela_precos.produto.id,
                TabelaPrecos.id != nova_tabela_precos.id,
                TabelaPrecos.ativa == True).update({'ativa': False})
            db.session.commit()

    def delete(self, id):
        try:
            TabelaPrecos.query.get(id).delete()
            db.session.commit()

        except exc.IntegrityError as err:
            return {'message': str(err)}, 401

