from flask_marshmallow import Marshmallow
from marshmallow import fields, validates, ValidationError, pre_load, Schema
from marshmallow_enum import EnumField

from project import app, Telefone, PessoaFisica, Vendedor, Usuario, Operadora, FaixaEtaria, Produto, TabelaPrecos, \
    Preco, Beneficiario, Venda, PerfilEnum, AcomodacaoEnum, StatusVendaEnum

ma = Marshmallow(app)


class TelefoneSchema(ma.ModelSchema):
    class Meta:
        model = Telefone


class PessoaFisicaSchema(ma.ModelSchema):
    id = fields.Int(required=False)
    cpf = fields.Str(required=True)
    #    cpf = fields.Str(required=True, validate=cpfcnpj.validate)
    email = fields.Email(missing=None)
    data_nascimento = fields.Date(required=True)
    telefones = fields.Nested(TelefoneSchema, many=True)

    class Meta:
        model = PessoaFisica


class VendedorSchema(ma.ModelSchema):
    id = fields.Int(required=True)
    pessoa_fisica = fields.Nested(PessoaFisicaSchema, only=['id', 'nome', 'cpf', 'data_nascimento', 'email'])
    ativo = fields.Bool(missing=True)

    class Meta:
        model = Vendedor


class UsuarioSchema(ma.ModelSchema):
    id = fields.Int(required=True)
    pessoa_fisica = fields.Nested(PessoaFisicaSchema, only=['id', 'nome', 'email', 'cpf', 'data_nascimento'])
    perfil = EnumField(PerfilEnum, required=True)

    @pre_load
    def hash_senha(self, in_data):
        in_data['senha'] = Usuario.gera_hash(in_data['senha'])

        return in_data

    class Meta:
        model = Usuario


class LoginSchema(Schema):
    login = fields.Str(required=True)
    senha = fields.Str(required=True)


class OperadoraSchema(ma.ModelSchema):
    ativo = fields.Bool(missing=True)

    class Meta:
        model = Operadora


class FaixaEtariaSchema(ma.ModelSchema):
    descricao = fields.Function(lambda obj: obj.descricao)

    class Meta:
        model = FaixaEtaria


class ProdutoSchema(ma.ModelSchema):
    descricao = fields.Function(lambda obj: obj.descricao)
    operadora_nome = fields.Function(lambda obj: obj.operadora.nome)
    acomodacao = EnumField(AcomodacaoEnum, required=True)
    operadora = fields.Nested(OperadoraSchema, required=True, only=('id', 'nome'), )
    ativo = fields.Boolean(missing=True)

    @validates('vidas')
    def validate_vidas(self, value):
        if value < 1 or value > 6:
            raise ValidationError('Cada produto deve ter entre 1 e 6 vidas (titular + dependentes)')

    class Meta:
        model = Produto


class PrecoSchema(ma.ModelSchema):
    faixa_etaria = fields.Nested(FaixaEtariaSchema, only=('id', 'descricao',))

    class Meta:
        model = Preco


class TabelaPrecosSchema(ma.ModelSchema):
    id = fields.Int(required=True)
    precos = fields.Nested(PrecoSchema, many=True, required=True)
    produto = fields.Nested(ProdutoSchema, only=('id', 'descricao',))

    class Meta:
        model = TabelaPrecos

    @validates('precos')
    def validate_precos(self, value):
        faixas_etarias = FaixaEtaria.query.all()
        if len(value) != len(faixas_etarias):
            raise ValidationError('Preço não informado para todas as faixas etárias')

        faixas_etarias_nao_informadas = [faixa_etaria for faixa_etaria in faixas_etarias if faixa_etaria.id not in
                                         [preco.faixa_etaria.id for preco in value]]
        if len(faixas_etarias_nao_informadas) > 0:
            raise ValidationError('Preço não informado para as faixas etárias: {}'.format(
                ', '.join([str(faixa_etaria) for faixa_etaria in faixas_etarias_nao_informadas])))


class BeneficiarioSchema(ma.ModelSchema):
    class Meta:
        model = Beneficiario


class VendaSchema(ma.ModelSchema):
    beneficiarios = fields.Nested(BeneficiarioSchema, many=True, required=True, exclude=('id', 'venda',))
    vendedor = fields.Nested(VendedorSchema, required=True, only=('id',))
    responsavel = fields.Nested(PessoaFisicaSchema, required=True, only=('id', 'nome', 'cpf', 'telefones',))
    status = EnumField(StatusVendaEnum, missing=StatusVendaEnum.CONFIRMADA)
    produto = fields.Nested(ProdutoSchema, required=True, only=('id',))

    class Meta:
        model = Venda


class Venda2Schema(ma.ModelSchema):
    beneficiarios = fields.Nested(BeneficiarioSchema, many=True, required=True, partial=('id',), exclude=('venda',))
    vendedor = fields.Nested(VendedorSchema, required=True, only=('id',))
    responsavel = fields.Nested(PessoaFisicaSchema, required=True,
                                only=('id', 'nome', 'cpf', 'telefones.id', 'telefones.numero'))
    status = EnumField(StatusVendaEnum, missing=StatusVendaEnum.CONFIRMADA)
    produto = fields.Nested(ProdutoSchema, required=True, only=('id', 'operadora.id','vidas'))

    class Meta:
        model = Venda
