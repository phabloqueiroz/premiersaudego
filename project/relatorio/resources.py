from flask import request, jsonify
from flask_restful import Resource

from project import db, excel, Vendedor


class RelatorioVendasResource(Resource):

    def get(self):
        data_inicio = request.args.get('data_inicio')
        data_fim = request.args.get('data_fim')

        query_select = """
            SELECT p2.nome        		vendedor, 
               Count(DISTINCT v.adesao) total_vendas, 
               Round(Sum(b2.preco), 2)  valor_total 
            FROM   venda v  
            INNER JOIN produto p 	    ON v.produto_id = p.id 
            INNER JOIN beneficiario b2  ON b2.venda_adesao = v.adesao 
            INNER JOIN vendedor v2      ON v2.id = v.vendedor_id
            INNER JOIN pessoa_fisica p2	ON v2.pessoa_fisica_id = p2.id
            WHERE  v.data BETWEEN STR_TO_DATE('{0}', '%Y-%m-%d') AND STR_TO_DATE('{1}', '%Y-%m-%d')
            GROUP  BY p2.nome """.format(data_inicio, data_fim)

        rs = db.engine.execute(query_select).fetchall()

        return jsonify([(dict(row.items())) for row in rs])


class RelatorioVendasPorOperadoraResource(Resource):

    def get(self):
        data_inicio = request.args.get('data_inicio')
        data_fim = request.args.get('data_fim')

        query_select = """
            SELECT p2.nome               vendedor,
                o.nome                   operadora,   
                Count(DISTINCT v.adesao) total_vendas, 
                Round(Sum(b2.preco), 2)  valor_total 
            FROM   venda v  
            INNER JOIN produto p 	    ON v.produto_id = p.id
            INNER JOIN operadora o      ON p.operadora_id = o.id 
            INNER JOIN beneficiario b2  ON b2.venda_adesao = v.adesao
            INNER JOIN vendedor v2      ON v2.id = v.vendedor_id
            INNER JOIN pessoa_fisica p2	ON v2.pessoa_fisica_id = p2.id 
            WHERE  v.data BETWEEN STR_TO_DATE('{0}', '%Y-%m-%d') AND STR_TO_DATE('{1}', '%Y-%m-%d')
            GROUP BY p2.nome, o.nome 
            ORDER BY p2.nome, o.nome""".format(data_inicio, data_fim)

        rs = db.engine.execute(query_select).fetchall()

        return jsonify([(dict(row.items())) for row in rs])


class RelatorioVendasPorVendedorResource(Resource):

    def get(self, vendedor_id):
        data_inicio = request.args.get('data_inicio')
        data_fim = request.args.get('data_fim')

        query_select = """
            SELECT
                v.adesao				            adesao,
                date_format(v.data, '%d/%m/%Y')		data,
                o.nome              	            operadora, 
                p.nome					            produto,
                Count(b2.id	) 			            vidas,
                p3.nome					            titular,
                Round(Sum(b2.preco), 2)             valor 
            FROM   venda v  
            INNER JOIN produto p 	    ON v.produto_id = p.id
            INNER JOIN operadora o      ON p.operadora_id = o.id 
            INNER JOIN beneficiario b2  ON b2.venda_adesao = v.adesao
            INNER JOIN vendedor v2      ON v2.id = v.vendedor_id
            INNER JOIN pessoa_fisica p2	ON v2.pessoa_fisica_id = p2.id 
            INNER JOIN beneficiario b3 	ON v.adesao = b3.venda_adesao
            INNER JOIN pessoa_fisica p3 ON b3.id = p3.id AND b3.e_titular = 1
            WHERE    v.data BETWEEN STR_TO_DATE('{0}', '%Y-%m-%d') AND STR_TO_DATE('{1}', '%Y-%m-%d')
            AND      v2.id = {2}
            GROUP BY v.data, o.nome, p.nome, v.adesao, p3.nome
            ORDER BY v.data, o.nome, p.nome""".format(data_inicio, data_fim, vendedor_id)

        vendedor = Vendedor.query.get(vendedor_id).pessoa_fisica.nome.lower().strip().replace(' ', '-')
        print(vendedor_id)

        return excel.make_response_from_query_sets(
            query_sets=db.engine.execute(query_select).fetchall(),
            file_type='xlsx',
            file_name='vendas_{0}_{1}_{2}'.format(data_inicio, data_fim, vendedor),
            column_names=['adesao', 'data', 'operadora', 'produto', 'vidas', 'titular', 'valor'])
