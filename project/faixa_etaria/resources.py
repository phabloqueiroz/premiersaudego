from flask_restful import Resource

from model import FaixaEtaria
from project.schema import FaixaEtariaSchema


class FaixaEtariaResource(Resource):

    def get(self):
        return FaixaEtariaSchema(many=True,).dump(FaixaEtaria.query.order_by(FaixaEtaria.id.asc()).all())
