from _operator import or_

from flask_restful import Resource

from model import TabelaPrecos, Preco, FaixaEtaria


class PrecoResource(Resource):

    def get(self, produto_id, idade):

        tabela_precos = TabelaPrecos.query\
            .filter(TabelaPrecos.produto_id == produto_id,
                    TabelaPrecos.ativa).first()

        preco = Preco.query\
            .join(FaixaEtaria, FaixaEtaria.id == Preco.faixa_etaria_id) \
            .filter(Preco.tabela_precos_id == tabela_precos.id,
                    FaixaEtaria.idade_minima <= idade,
                    or_(FaixaEtaria.idade_maxima >= idade,
                        FaixaEtaria.idade_maxima == None)).first()

        return preco.preco
