from flask import request, jsonify
from flask_restful import Resource
from marshmallow import ValidationError
from sqlalchemy import exc

from model import StatusVendaEnum
from project import Venda, db
from ..schema import VendaSchema, Venda2Schema


class VendaStatusResource(Resource):

    def delete(self, id):
        venda = Venda.query.get(id)
        venda.status = StatusVendaEnum.CANCELADA
        venda.comissionada = request.args['comissionada']


class Venda2Resource(Resource):

    def get(self, adesao):
        return jsonify(Venda2Schema().dump(Venda.query.get(adesao)))


class VendaResource(Resource):

    def get(self, adesao=None):

        query_select = """SELECT v.adesao, v.data, 
                       o.nome                  operadora, 
                       p.nome                  produto, 
                       pf.nome                 titular, 
                       Count(*)                vidas, 
                       Round(Sum(b2.preco), 2) valor 
                FROM   venda v 
                       INNER JOIN beneficiario t ON t.venda_adesao = v.adesao AND t.e_titular = 1 
                       INNER JOIN pessoa_fisica pf ON t.id = pf.id 
                       INNER JOIN produto p ON v.produto_id = p.id 
                       INNER JOIN operadora o ON o.id = p.operadora_id 
                       INNER JOIN beneficiario b2 ON b2.venda_adesao = v.adesao """

        query_group_by = "GROUP  BY v.adesao, v.    data, o.nome, p.nome, pf.nome ORDER BY v.data, v.adesao; "

        if adesao:
            query_where = "WHERE v.adesao = {} ".format(adesao)
        else:
            query_where = "WHERE 1 = 1 "

            operadora_id = request.args.get('operadora_id')
            produto_id = request.args.get('produto_id')
            vendedor_id = request.args.get('vendedor_id')
            data_inicio = request.args.get('data_inicio')
            data_fim = request.args.get('data_fim')

            if operadora_id and not produto_id:
                query_where += "AND o.id = {} ".format(operadora_id)

            if produto_id:
                query_where += "AND p.id = {} ".format(produto_id)

            if vendedor_id:
                query_where += "AND v.vendedor_id = {} ".format(vendedor_id)

            if data_inicio:
                query_where += "AND v.data >= STR_TO_DATE('{}', '%Y-%m-%d') ".format(data_inicio)

            if data_fim:
                query_where += "AND v.data <= STR_TO_DATE('{}', '%Y-%m-%d') ".format(data_fim)

        rs = db.engine.execute(query_select + query_where + query_group_by).fetchall()

        return jsonify([(dict(row.items())) for row in rs])

        # return VendaSchema(many=True).dumps(query.order_by(Venda.data.desc(), Venda.adesao.desc()).all())

    def put(self):
        venda_schema = VendaSchema()
        try:
            venda = venda_schema.load(request.json)

            db.session.commit()

        except ValidationError as err:
            return {'message': err.messages}, 401

        except exc.IntegrityError:
            return {'message': 'Erro ao atualizar registro: '}, 401

    def post(self):
        venda_schema = VendaSchema()
        try:

            venda = venda_schema.load(request.json)

        except ValidationError as err:
            return {'message': err.messages}, 401

        db.session.add(venda)
        db.session.commit()
